module "sg" {
  source = "../modules/sg"

  name                   = "demo-sg"
  description            = "Creates security group"
  vpc_id                 = module.vpc.vpc_id
  project                = "demo"

}

module "vpc" {
  source = "../modules/vpc"
  cidr_block             = "10.0.0.0/16"
  project                   = "demo"

}

