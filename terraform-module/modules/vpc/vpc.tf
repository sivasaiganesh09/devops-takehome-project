resource "aws_vpc" "demo" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "${var.project}-vpc"
  }
}