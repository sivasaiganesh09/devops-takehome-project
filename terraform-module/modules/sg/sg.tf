

##########################
# Security group with name
##########################
resource "aws_security_group" "demo" {

  name                   = var.name
  description            = var.description
  vpc_id                 = var.vpc_id


  tags = {
    Name = "${var.project}-sg"
  }
}


