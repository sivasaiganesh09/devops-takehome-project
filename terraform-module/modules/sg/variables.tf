#################
# Security group
#################

variable "name" {
  description = "Name of security group"
  type        = string
  default     = null
}

variable "description" {
  description = "Description of security group"
  type        = string
  default     = "Security Group managed by Terraform"
}

variable "vpc_id" {
  description = "ID of the VPC where to create security group"
  type        = string
  default     = null
}

variable "project" {
  description = "Tag name"
  type        = string
  default     = null
}







