## It uses node js image alpine version from image registries.
FROM node:lts-alpine3.14
## It sets directory in the container to /app to store files and launch our app.
WORKDIR /app
## It copies the app to /app directory with dependencies.
COPY ./website/package.json /app
RUN npm install
COPY ./website /app
## It commands to run our app which is node.js.
CMD node node.js
##  It exposes the port where our app is running that is port 3000.
EXPOSE 3000